package angularSearch.colorAnalysis;

import java.awt.Color;
import java.awt.image.BufferedImage;

import angularSearch.names.ColorRange;
import angularSearch.names.Season;

public class ImageCalculator {

	BufferedImage image;
	CounterRange counterR;
	CounterSeason counterS;
	
	private Season season;
	private ColorRange range;
	
	public ImageCalculator() { // for test purpose only
	}

	public ImageCalculator (BufferedImage image) {
		this.image=image;
		counterR = new CounterRange ();
		counterS = new CounterSeason ();
		analyzeImage ();
		calculateSeason ();
		calculateColorRange ();
	}
	
	private void calculateColorRange() {
		
		System.out.println(this);
		
		while (true) {
		  
			if (counterR.getColor()>1000) {
				this.range=ColorRange.COLOR;
				break;
			}
			
			if (counterR.getPastel()>1000 && counterR.getPastel()>counterR.getSepia()) {
				this.range=ColorRange.PASTEL;
				break;
			}
		
			if (counterR.getSepia()>1000 ) {
				this.range=ColorRange.SEPIA;
				break;
			}
			
			if (counterR.getBlack()>0.1*(counterS.getSpring() + counterS.getAutumn() )) {
				this.range=ColorRange.WHITE;
				break;
			}
			
			this.range=ColorRange.OTHER;
			break;
		}
	}

	private void calculateSeason() {
		int max = Math.max(counterS.getAutumn(), counterS.getSummer());
		max = Math.max(max, counterS.getSpring());
		
		while (true) {
			
		    if (counterS.getAutumn()==max) { 
		    	this.season=Season.AUTUMN;
		    	break;
		    }
		
		
		    if (counterS.getSpring()==max || counterS.getSpring() > 200000) { 
		    	this.season=Season.SPRING;
		    	break;
		    }
		
		    this.season=Season.SUMMER;
		    break;
		
		}
		
	}

	public Season getSeason() {
		checkIfDefined ();
		return season;
	}

	public ColorRange getRange() {
		checkIfDefined ();
		return range;
	}
	
	private void checkIfDefined () {
	    if (image==null) throw new RuntimeException("Image is not provided for analysis in the constructor. "
	    		+ "Empty constructor is only for testing");
	    if (this.season==null || this.range==null) throw new RuntimeException ("It was forgotten to calculate season and/or color range");
	}
	
public String testPixelColor (Color pixel) {
	testMethod(pixel);
	return "b"+counterR.getBlack()+"s"+counterR.getSepia()+"p"+counterR.getPastel()+"c"+counterR.getColor();
}


public String testPixelAutumnSpring (Color pixel) {
	testMethod(pixel);
	return "a"+counterS.getAutumn()+"s"+counterS.getSpring();
}

private void testMethod(Color pixel) {
	this.counterR = new CounterRange();
	this.counterS = new CounterSeason();
	countPixel(pixel);
}

private void countPixel(Color pixel) {
	Converter pix = new Converter (pixel);
	ColorUtility.addPixelToCounter(pix, counterR);
	SeasonUtility.addPixelToCounter(pix, counterS);
}

private void analyzeImage() {
	for (int x = 0; x < image.getWidth(); x++) {
        for (int y = 0; y < image.getHeight(); y++) {
        	countPixel(new Color(image.getRGB(x, y)));
        }
	}
}

@Override
public String toString() {
	return "ImageCalculator [counterR=" + counterR + ", counterS=" + counterS + ", season=" + season + ", range="
			+ range + "]";
}

	

	
	
}
