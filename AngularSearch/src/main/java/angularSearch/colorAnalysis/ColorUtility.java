package angularSearch.colorAnalysis;

public class ColorUtility {

	private final static ColorUtility single = new ColorUtility ();
	
	private final static int [] pastel_Range = {180, 200, 220, 240, 250};
	private final static int deviation = 5;
	
	private ColorUtility () {}
	
	public static void addPixelToCounter (Converter pix, CounterRange  counter) {
		single.addRangeCounters(pix, counter);
	}
	
	private void addRangeCounters(Converter pix, CounterRange counter) {
  		chechAndAddBlack (pix, counter);
		chechAndAddSepia (pix, counter);
		chechAndAddPastel (pix, counter);
		chechAndAddColor (pix, counter);
	}
	
	private void chechAndAddPastel (Converter pix, CounterRange counter) {
		
		if (isPastel (pix.getR()) && isPastel (pix.getG()) && isPastel (pix.getB())) {
			int redder = Math.abs(pix.getR() - pix.getG());
			int greener = Math.abs(pix.getR() - pix.getG());
			boolean first = redder < 65;
			boolean second = greener < 65;
			boolean notGrey = greener + redder>=15;
			
			if (first && second && notGrey) counter.addPastel();
		}
		
	}


	private boolean isPastel(int rgb) {
		for (int i=0; i<pastel_Range.length; i++) {
			if (rgb >= pastel_Range[i] - deviation && rgb <= pastel_Range[i] + deviation) { 
				return true;
			}
		}
		return false;
	}


	private void chechAndAddColor(Converter pix, CounterRange counter) {
		if (pix.getS()>=90 && pix.getBr()>=90) counter.addColor();
	}


	private void chechAndAddSepia(Converter pix, CounterRange counter) {
		int greener = pix.getG() - pix.getB();
		int redder = (pix.getR() - pix.getB()) / 2;
		if (greener >= 10 && redder>=10) {
			int percent = (int) (Math.abs(redder - greener) * 100 / greener);
			if (percent < 5) counter.addSepia();
		}
		
	}


	private void chechAndAddBlack(Converter pix, CounterRange counter) {
		int avg = (pix.getR() + pix.getG() + pix.getB()) / 3;
		if (Math.abs(avg)>1) {
		
		  boolean first = (int) (Math.abs(pix.getR() - avg) * 100 / avg) <=2;
		  boolean second = (int) (Math.abs(pix.getG() - avg) * 100 / avg) <=2;
		  boolean third = (int) (Math.abs(pix.getB() - avg) * 100 / avg) <=2;
		
		  if (first && second && third) counter.addBlack();}
		else counter.addBlack();
	}
	
}
