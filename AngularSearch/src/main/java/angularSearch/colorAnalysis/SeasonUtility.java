package angularSearch.colorAnalysis;

public class SeasonUtility {

	private final static SeasonUtility single = new SeasonUtility ();
	
	private SeasonUtility () {}
	private final int[][] winterBlue = {
			{221,221,221}, {187,187,187}, {161,215,236}, {139,187,206}
	};
	
	public static void addPixelToCounter (Converter pix, CounterSeason  counter) {
		single.addSeasonCounters(pix, counter);
	}
	
	
	private void addSeasonCounters(Converter pix, CounterSeason  counter) {
		
		while (true) {
		
			if (isAutumnAdded (pix, counter)) break;
			if (isSpringAdded (pix, counter)) break;
			counter.addSummer(); break;
		}
	}


	private boolean isAutumnAdded (Converter pix, CounterSeason  counter) {
		boolean notBright = pix.getBr()<=60;
		boolean notBlack = pix.getS()>30 && pix.getBr()>20;
		boolean orangeOrYellow = pix.getH() < 60;
		boolean red = pix.getH() > 340; 
		
		if (notBlack ) {
		  if (orangeOrYellow || red && notBright) {
			  counter.addAutumn();
			  return true;
		  }
		}
		return false;
	}


	private boolean isSpringAdded (Converter pix, CounterSeason  counter) {
		boolean bright = pix.getBr()>=65;
		boolean greenColor = pix.getH() >= 75 && pix.getH() <= 145;
		//boolean saturation = pix.getS() > 50;
		
		  if (bright && greenColor) {
			counter.addSpring();
			return true;
		  }
		
		return false;
	}
	


}
