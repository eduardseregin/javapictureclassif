package angularSearch.colorAnalysis;

public class CounterSeason {

	private int spring=0;
	private int autumn=0;
	private int summer=0;
	
	public CounterSeason() {
		super();
	}

	public int getSpring() {
		return spring;
	}

	public void addSpring() {
		this.spring ++;
	}

	public int getSummer() {
		return summer;
	}

	public void addSummer() {
		this.summer ++;
	}

	public int getAutumn() {
		return autumn;
	}

	public void addAutumn() {
		this.autumn ++;
	}


	@Override
	public String toString() {
		return "CounterSeason [spring=" + spring + ", summer=" + summer + ", autumn=" + autumn  + "]";
	}

		
}
