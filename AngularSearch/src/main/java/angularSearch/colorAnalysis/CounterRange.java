package angularSearch.colorAnalysis;

public class CounterRange {

	private int black=0;
	private int sepia=0;
	private int pastel=0;
	private int color=0;
	
	public CounterRange() {
	}

	public int getBlack() {
		return black;
	}

	public void addBlack() {
		this.black ++;
	}

	public int getSepia() {
		return sepia;
	}

	public void addSepia() {
		this.sepia++;
	}

	public int getPastel() {
		return pastel;
	}

	public void addPastel() {
		this.pastel ++;
	}

	public int getColor() {
		return color;
	}

	public void addColor() {
		this.color++;
	}

	@Override
	public String toString() {
		return "CounterRange [black=" + black + ", sepia=" + sepia + ", pastel=" + pastel + ", color=" + color + "]";
	}
	

	
}
