package angularSearch.colorAnalysis;

import java.awt.Color;

public class Converter {

	private int tempRed;
	private int tempGreen;
	private int tempBlue;
	private int tempHue;
	private int tempSatPercent;
	private int tempBrightPercent;
	
	public Converter (Color pixel) {
		assignRGB(pixel);
		assignHSB();
	}
	
	
	private void assignRGB(Color pixel) {
		this.tempRed = pixel.getRed();
		this.tempGreen = pixel.getGreen();
		this.tempBlue = pixel.getBlue();
	}
	
	private void assignHSB() {
		float[] hsb = Color.RGBtoHSB(this.tempRed, this.tempGreen, this.tempBlue, null);
		this.tempHue = (int) (hsb [0] * 360);
		this.tempSatPercent = (int) (hsb [1] * 100);
		this.tempBrightPercent = (int) (hsb [2] * 100);
	}


	public int getR() {
		return tempRed;
	}


	public int getG() {
		return tempGreen;
	}


	public int getB() {
		return tempBlue;
	}


	public int getH() {
		return tempHue;
	}


	public int getS() {
		return tempSatPercent;
	}


	public int getBr() {
		return tempBrightPercent;
	}
	
	
}
