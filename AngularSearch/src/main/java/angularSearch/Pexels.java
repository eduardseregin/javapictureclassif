package angularSearch;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Pexels {

	
	public static BufferedImage getImage (int id) {
	    
		String url = getImgUrl (id);
		
		BufferedImage image = new BufferedImage (1,1,5);
		
		Response res = getResponse(url); 
	       
		if (res != null) {

			try {
				InputStream is = res.bodyStream();
				image = ImageIO.read(is);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
        return image;
	}
	
	public static String getKeywords (int id) {
		
	   String url = getKWUrl (id);
       StringBuilder keywords = new StringBuilder ("");
	
	   Response res = getResponse(url); 
       
		if (res != null) {
			try {
				Document doc;
				doc = res.parse();

				keywords.append(doc.getElementsByTag("title").text() + " ");
				Elements elems = doc.getElementsByTag("meta");

				for (Element e : elems) {
					if (e.attr("name").equals("description"))
						keywords.append(e.attr("content") + " ");
					if (e.attr("name").equals("keywords"))
						keywords.append(e.attr("content"));
				}
			} catch (IOException e1) {
				System.out.println("doc cannot be retreved from " + url);
			}
		}
		return keywords.toString();
	}
	
	
	private static Response getResponse (String url) {
		Connection store = Jsoup.connect (url).cookie(url, url);
		
		Response res = null;
		try {

			res = store.timeout(5000)
			        .userAgent("Mozilla")
			        .referrer("http://www.google.com")
			        .followRedirects(true)
			        .ignoreHttpErrors(true)
			        .ignoreContentType(true)
			        .execute();
		} catch (IOException e) {
			System.out.println("Connection to url: " + url + " is not possible");
			System.out.println(e);;
		}
		
		return res;

	}
	
	private static String getKWUrl (int id) {
		return "https://www.pexels.com/photo/"+ id+"/";
	}
	
	private static String getImgUrl (int id) {
		return "https://images.pexels.com/photos/"+id+"/pexels-photo-"+
	id+".jpeg?auto=compress";
	}
	
}
