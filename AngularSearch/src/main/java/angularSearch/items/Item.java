package angularSearch.items;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import angularSearch.Accumulator;
import angularSearch.colorAnalysis.ImageCalculator;
import angularSearch.names.ColorRange;
import angularSearch.names.Season;
import angularSearch.words.DescribingWords;
import angularSearch.words.KeywordsCalculator;

public class Item implements Comparable <Item> {

	private static List <Item> items = new ArrayList <>();
	private static Accumulator accum = new Accumulator ();
	
	private int id;
	
	private Flags flags = new Flags ();

	private Season season;
	private ColorRange range;
	
	private DescribingWords words;

	// toDELETE
	
	public Item (int id, ColorRange range, Season season, Flags flags) {
		this.id = id;
		this.range = range;
		this.season = season;
		this.flags = flags;
		items.add(this);
	}
	
	// END OF DELETE
	
	private Item (int id, String keywords, BufferedImage image) {
		this.id = id;
		KeywordsCalculator.addKeywordsToFlags(keywords, flags);
		ImageCalculator imgc = new ImageCalculator(image);
		this.season = imgc.getSeason();
		this.range = imgc.getRange();
		this.words = DescribingWords.instance();
	}
	
	public static List <Item> getItems () {
		List <Item> copy = new ArrayList <> ();
		for (Item item: items) copy.add(item);
		return copy;
	}
	
	public static int size () {
		return items.size();
	}
	
	public static Accumulator getAccum () {
		return accum;
	}
	
	public static void pushItem (int id, String keywords, BufferedImage image) {
		Item item = new Item (id, keywords, image);
		if (item.isNotZero(keywords, image)) {
			items.add(item);
			accum.addItem(item);
		}
	}
	
	private boolean isNotZero(String keywords, BufferedImage image) {
		
		boolean keywordsNotEmpty = keywords!=""; 
		boolean hasValue = words.getList().stream().anyMatch(x->keywords.contains(x));
		boolean imageNotEmpty = image.getHeight()>1000;
		
		if (keywordsNotEmpty && hasValue && imageNotEmpty) return true;
		return false;
	}

	
	@Override
	public String toString() {
		String qte = "\"";
		return "{" + 
	
	  qte+"id"+qte+":" + this.id + "," +
	  qte+"color"+qte+":" +qte + this.range +qte + "," +
	  qte+"season"+qte+":" +qte + this.season +qte + "," +
	  qte+"nature"+qte+":" + this.flags.isNature() + "," +
	  qte+"city"+qte+":" + this.flags.isCity() + "," +
	  qte+"people"+qte+":" + this.flags.isPeople() + "," +
	  qte+"fantasy"+qte+":" + this.flags.isFantasy() + "," +
	  qte+"jokes"+qte+":" + this.flags.isJokes() + "," +
	  qte+"films"+qte+":" + this.flags.isFilms() + "," +
	  qte+"tech"+qte+":" + this.flags.isTech() + "," +
		
      qte+"happy"+qte+":" + this.flags.isHappy() + "," +
      qte+"excited"+qte+":" + this.flags.isExcited() + "," +
      qte+"tender"+qte+":" + this.flags.isTender() + "," +
      qte+"grim"+qte+":" + this.flags.isGrim() + "," +
      qte+"sad"+qte+":" + this.flags.isSad() 
		
				+ "},";
	
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flags == null) ? 0 : flags.hashCode());
		result = prime * result + id;
		result = prime * result + ((range == null) ? 0 : range.hashCode());
		result = prime * result + ((season == null) ? 0 : season.hashCode());

		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (flags == null) {
			if (other.flags != null)
				return false;
		} else if (!flags.equals(other.flags))
			return false;
		if (range != other.range)
			return false;
		if (season != other.season)
			return false;
		
		return true;
	}

	@Override
	public int compareTo(Item o) {
		if (o.equals(this)) return 0;
	    if (this.season.compareTo(o.season)>0) return 1;
	    if (this.season.compareTo(o.season)<0) return -1;


			if (this.range.compareTo(o.range)>0) return 1;
			if (this.range.compareTo(o.range)<0) return -1;
			
		if (this.flags.compareTo(o.flags)>0) return 1;
		if (this.flags.compareTo(o.flags)<0) return -1;
		
		if (this.id>o.id) return 1;
		
		return -1;
	}

	public int getId() {
		return id;
	}

	public Flags getFlags() {
		return flags;
	}

	public Season getSeason() {
		return season;
	}

	public ColorRange getRange() {
		return range;
	}
	
	
	
}
