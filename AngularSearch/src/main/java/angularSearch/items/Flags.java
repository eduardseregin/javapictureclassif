package angularSearch.items;

public class Flags implements Comparable <Flags> {
	

    private boolean nature;
	private boolean city;
	private boolean people;
	private boolean fantasy;
	private boolean jokes;
	private boolean films;
	private boolean tech;
	private boolean happy;
	private boolean excited;
	private boolean tender;
	private boolean grim;
	private boolean sad;

	public Flags() {
		this.nature = false;
		this.city = false;
		this.people = false;
		this.fantasy = false;
		this.jokes = false;
		this.films = false;
		this.tech = false;
		this.happy = false;
		this.excited = false;
		this.tender = false;
		this.grim = false;
		this.sad = false;
	}

	public void setFlag (int index) {
		boolean value = true;
		switch (index) {
		case 0: this.nature = value; break;
		case 1: this.city = value; break;
		case 2: this.people = value; break;
		case 3: this.fantasy = value; break;
		case 4: this.jokes = value; break;
		case 5: this.films = value; break;
		case 6: this.tech = value; break;
		case 7: this.happy = value; break;
		case 8: this.excited = value; break;
		case 9: this.tender = value; break;
		case 10: this.sad = value; break;

		default: throw new RuntimeException ("Index for listing is not correctly stated");
		}
	}
	
	public void setGrim(boolean grim) { // grim is set as "the rest"
		this.grim = grim;
	}

	public boolean isNature() {
		return nature;
	}

	public boolean isCity() {
		return city;
	}

	public boolean isPeople() {
		return people;
	}

	public boolean isFantasy() {
		return fantasy;
	}

	public boolean isJokes() {
		return jokes;
	}

	public boolean isFilms() {
		return films;
	}

	public boolean isTech() {
		return tech;
	}

	public boolean isHappy() {
		return happy;
	}

	public boolean isExcited() {
		return excited;
	}

	public boolean isTender() {
		return tender;
	}

	public boolean isGrim() {
		return grim;
	}

	

	public boolean isSad() {
		return sad;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (city ? 1231 : 1237);
		result = prime * result + (excited ? 1231 : 1237);
		result = prime * result + (fantasy ? 1231 : 1237);
		result = prime * result + (films ? 1231 : 1237);
		result = prime * result + (grim ? 1231 : 1237);
		result = prime * result + (happy ? 1231 : 1237);
		result = prime * result + (jokes ? 1231 : 1237);
		result = prime * result + (nature ? 1231 : 1237);
		result = prime * result + (people ? 1231 : 1237);
		result = prime * result + (sad ? 1231 : 1237);
		result = prime * result + (tech ? 1231 : 1237);
		result = prime * result + (tender ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Flags other = (Flags) obj;
		if (city != other.city)
			return false;
		if (excited != other.excited)
			return false;
		if (fantasy != other.fantasy)
			return false;
		if (films != other.films)
			return false;
		if (grim != other.grim)
			return false;
		if (happy != other.happy)
			return false;
		if (jokes != other.jokes)
			return false;
		if (nature != other.nature)
			return false;
		if (people != other.people)
			return false;
		if (sad != other.sad)
			return false;
		if (tech != other.tech)
			return false;
		if (tender != other.tender)
			return false;
		return true;
	}

	@Override
	public int compareTo(Flags o) {
		if (o.equals(this)) return 0;

		if (this.getFlagMoodIndex()<o.getFlagMoodIndex()) return 1;
		if (this.getFlagMoodIndex()>o.getFlagMoodIndex()) return -1;
		
		if (this.getFlagTopicIndex()<o.getFlagTopicIndex()) return 1;
		if (this.getFlagTopicIndex()>o.getFlagTopicIndex()) return -1;

			return 0;
	}

	private int getFlagTopicIndex () {
		
		for (int i=0; i<=6; i++) {
			if (flagByIndexTopic (i)) return i;
		}
		
		return 7;
	}
	
	private int getFlagMoodIndex () {
	
		for (int i=0; i<=6; i++) {
			if (flagByIndexMood (i)) return i;
		}
		
		return 5;
	}
	

	
	private boolean flagByIndexTopic (int index) {
		switch (index) {
		case 0: return nature;
		case 1: return city;
		case 2: return people;
		case 3: return fantasy;
		case 4: return jokes;
		case 5: return films;
		case 6: return tech;
		
		default: throw new RuntimeException ("Flags comparing index is out of range of 6: " + index);
		}
	}
	
	private boolean flagByIndexMood (int index) {
		switch (index) {
		case 0: return happy;
		case 1: return excited;
		case 2: return tender;
		case 3: return grim;
		case 4: return sad;

		
		default: throw new RuntimeException ("Flags comparing index is out of range of 4: " + index);
		}
	}
	
}