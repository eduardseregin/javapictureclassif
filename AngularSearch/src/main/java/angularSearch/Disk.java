package angularSearch;

import java.io.FileOutputStream;
import java.util.List;
import java.util.stream.Collectors;

import angularSearch.items.Item;

public class Disk {

	public static void toFile (List <Item> items) {
		String path = "C:\\Temp\\forAngular\\sourceData.txt";
		save (path, itemsToString(items));
		
	}
	
	public static void save (String path, List <String> strings) {

		try {
		
		FileOutputStream fileOutputStream = new FileOutputStream(path);

	     for (String s: strings) {
	       s+="\n";
	       fileOutputStream.write(s.getBytes());
	     }

	    fileOutputStream.close();
	    
		} catch (Exception e) {System.out.println(e);}
	}
	
	private static List <String> itemsToString (List <Item> items) {
		return items.stream().map(x->x.toString()).collect(Collectors.toList());
	}
	

}
