package angularSearch.words;

import java.util.List;

import angularSearch.items.Flags;

public class KeywordsCalculator {

	private final static KeywordsCalculator single = new KeywordsCalculator ();
	private DescribingWords words;
	
	private KeywordsCalculator () {
		words = DescribingWords.instance();
	}
	
	public static void addKeywordsToFlags (String keywords, Flags flags) {
		single.checkAndSetFlags (keywords, flags);
	}
	
	private void checkAndSetFlags (String keywords, Flags flags) {
		boolean isGrim = true; // default
		for (int i=0; i<=10;i++) {
			if (containsWords(i, keywords)) {
				flags.setFlag (i);
				if (i>=7) isGrim=false;
			}
		}
		if (isGrim) flags.setGrim(true);
	}
	
	private boolean containsWords (int index, String keywords) {

		String keywordsToLow = keywords.toLowerCase();
		List <String> wordsToCheck = words.getList(index);
		for (String word: wordsToCheck) {
			word = word.toLowerCase();
			if (keywordsToLow.contains(word)) {
				System.out.println(word);
				return true;
			}
		}
		return false;
	}
	
}
