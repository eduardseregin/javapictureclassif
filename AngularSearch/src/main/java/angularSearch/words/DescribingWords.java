package angularSearch.words;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class DescribingWords {

	private static DescribingWords single;
	
	
	private List <String> nature;
	private List <String> city;
	private List <String> people;
	private List <String> fantasy;
	private List <String> jokes;
	private List <String> films;
	private List <String> tech;

	private List <String> happy;
	private List <String> excited;
	private List <String> tender;
	private List <String> sad;
	
	private List <String> every;
	


	private DescribingWords() {
		setEverything ();
	}

	public static DescribingWords instance () {
		if (single == null) single=new DescribingWords();
		return single;
	}
	
	public List <String> getList (int index) {
		switch (index) {
		case 0: return this.nature;
		case 1: return this.city;
		case 2: return this.people;
		case 3: return this.fantasy;
		case 4: return this.jokes;
		case 5: return this.films;
		case 6: return this.tech;
		case 7: return this.happy;
		case 8: return this.excited;
		case 9: return this.tender;
		case 10: return this.sad;
		
		default: throw new RuntimeException ("Index for listing is not correctly stated");
		}
	}
	
	public List <String> getList () {
		return this.every;
	};
	
	private void setEverything() {

		setNature ();
		setCity();
		setPeople();
		setFantasy();
		setJokes();
		setFilms();
		setTech();

		setHappy();
		setExcited();
		setTender();
		setSad();		
		
		every = new ArrayList<>();
		IntStream.range(0,10).forEach (i->every.addAll(getList(i)));
	}

	private void setSad() {
		this.sad=List.of("ugly", "old", "sad", "bored", "disappointed", "tired", "weary", "annoyed", 
				"lazy", "absent-minded", "obedient", "snobbish", "boring", "cautious", "dense", "wooded",
				"exquiste", "quaint", "boring", "dilapidated", "derelict", "demolish", "urban", "poor", 
				"silly", "violent", "flawed", "weak", "static", "confused", "outdated", "flawed", 
				"predictable", "uneven", "avoid", "empty", "hesitant", "hesitate", "indifferent", "inferior", 
				"insecure", "worried", "worry", "obsolete", "satisfact", "stupid", "slow", "storm", "wind", "vibration",
				"dark", "scare", "broken", "closed", "hidden", "remote");
		
	}

	private void setExcited() {
		this.excited=List.of("excited","attractive", "pretty", "redhead", "excited", "delighted", "surprised", 
				"enthusiastic", "clever", "funny", "witty", "sophisticated", "modest", "brave", "cowardly", 
				"delightful", "fascinating", "fantastic", "thriving", "stunning", "renovate", "gorgeous", 
				"significant", "heritage", "chic", "powerful", "legend", "intriguing", "charismatic", "satirical", 
				"energy", "energetic", "interest", "provocative");
		
	}

	private void setTender() {
		this.tender=List.of("smart", "young", "blonde", "confident", "sensitive", "calm", "moody", "arrogant", 
				"shy", "sensible", "serious", "honest", "mean", "intelligent", "polite", "friendly", "peaceful", 
				"vivid", "vibrant", "elegant", "flourishing", "spectacular", 
				"charming", "preserve", "cultural", "restore", "tender", "original", "sensitive", "sense", 
				"slow", "romantic", "buoyant", "judge", "vulnerable");
		
	}

	private void setHappy() {
		this.happy=List.of("beautiful", "handsome", "brunette", "happy", 
				"pleased", "relaxed", "cheerful", "generous", "kind", "talented", "renewed", "lush", 
				"crisp", "pure", "bright", "sweetly", "aromatic", "alive", "beautiful", "floral", "grassy", 
				"heavenly", "delighful", "awesome", "popular", "famous", "ancient", "vibrant", "lively", 
				"comical", "enjoy", "dazzling", "fantasy", "joy", "amuse", "optimism", "optimistic", 
				"play", "proud", "crack", "love", "laugh", "tease");
		
	}

	private void setTech() {
		this.tech=List.of("tech", "engineering", "mechanics", "robot", "computer", "information", 
				"mathematics", "machinery", "machine", "chemistry", "chemical", "test", "apparatus", 
				"equipment", "device", "facility", "tool", "tackle", "mechanism", "system", "scheme", 
				"fixture", "plant", "system", "gear", "wheel", "gearing", "mesh", "instrument", "ploy", 
				"gadget", "appliance", "artifice", "contraption", "processor", "process", "automation",
				"bot", "drone", "buzz", "whine", "purr", "bionic", "control", "electronic", "electricity", 
				"laptop", "desktop", "software", "hardware", "plug", "cork", "wad", "socket", "bung", 
				"liquid", "fluid", "molten", "engine", "gizmo", "aircraft", "train", "vehicle",
				"car", "van", "coach", "trunk", "trolley", "chariot", "sedan", "automobile", "drive", "moto",
				"auto", "cart", "oil", "lubricate", "lightening", "repair", "fix", "reparation", "refurbishment", 
				"renovation", "restoration", "fasten", "patch", "blotch", "shred", "utensil", "vessel", 
				"accessory", "supplement", "add-on");
		
	}

	private void setFilms() {
		this.films=List.of("cartoon", "animation", "film", "movie", "animated", "graphic", "lampoon", "mimic", 
				"illustration", "doodle", "painting", "draw", "depict", "anime", "Naruto", "cosplay", 
				"manga", "hentai", "yaoi", "doujinshi");
		
	}

	private void setJokes() {
		this.jokes=List.of("joke", "gag", "jest", "fun", "prank", "mock", "trick", "laugh", "muzzle", 
				"farce", "ridicule", "parody", "jape", "banter", "tease", "fool", "butt", "derision", 
				"lark", "spoof", "humor", "comic", "slapstick", "deadpan", "potty", "satire", "comedy", 
				"stand-up", "meme", "troll", "skit", "amateur", "rant", "gifs", "fad", "craze", "ironical", 
				"irony", "cool", "absurd", "buzzword", "anime", "Naruto", "cosplay", "manga", "hentai", "yaoi", 
				"doujinshi", "charade", "sketch", "burlesque", "spoof", "caricature");
		
	}

	private void setFantasy() {
		this.fantasy=List.of("fantasy", "fairy", "imagination", "illusion", "dream", "chimera", "hallucination", 
				"deception", "fallacy", "mirage", "impression", "phantasm", "fancy", "magic", "pixie", "elf", 
				"faerie", "leprechaun", "king", "sprite", "legend", "whimsical", "fantastic", "unreal", 
				"bizarre", "exotic", "kinky", "imagined", "illusory", "daring", "whimsical", "utopia", "visionary",
				"unrealistic", "tear-jerking", "idyllic", "folkloric", "curious");
		
	}

	private void setCity() {
		this.city=List.of("city", "town", "building", "house", "village", "municipal", "burg", "district", 
				"community", "civic", "metropolitan", "neighborhood", "public", "vicinity", "locale", 
				"suburb", "megapolis", "country", "metropolis", "capital", "borough", "center", 
				"conurbation", "urban", "state", "industrial", "architecture", "office", "domicile", 
				"construction", "erection", "factory", "fabrication", "shop", "mall", "supermarket", 
				"outlet", "avenue", "road", "pathway", "alley", "passage", "parking", "boulevard",
				"address", "plaza", "promenade", "center", "centre", "arcade", "grocery", "boutique", 
				"borough", "region", "province", "quarter", "area");
		
	}

	private void setPeople() {
		this.people=List.of("men", "woman", "women", "child", "children", "person", 
				"people", "lady", "female", "adult", "princess", "babe", "baby", "mistress", "police", "girl",
				"madam", "baroness", "maid", "lord", "employee", "business", "business", "worker", "professor", 
				"prist", "payer", "prayer", "teacher", "interpreter", "waiter", "taxi driver", "driver", 
				"engineer", "lawyer", "accountant", "manager", "consultant", "administrator", "economist", 
				"financier", "builder", "nurse", "secretary", "vet", "scientist", "dentist", "writer", 
				"musician", "actor", "artist", "singer", "stylist", "barber", "hairdresser", "photographer", 
				"journalist", "composer", "courier", "shop assistant", "guide", "designer", "programmer", 
				"architect", "coach", "athlete", "barman", "bodyguard", "cashier", "cleaner", "cook", 
				"electrician", "plumber", "librarian", "doctor", "policeman", "fireman", "military (man)", 
				"politician", "postman", "priest", "Cosmonaut", "Seller", "Pilot", "Architect", "Librarian");
		
	}

	private void setNature() {
		this.nature=List.of("Canal", "Bridge", "Dam", "Lighthouse", "Island", "Bay", "Riverbank", 
				"Beach", "water", "Ocean", "Coast", "Ground", "Dune", "Desert", "Cliff", "Park", 
				"Meadow", "Jungle", "Forest", "Glacier", "Land", "Hill", "Field", "Grass", 
				"Soil", "Sea shell", "Mushroom", "Pebble", "Rock", "Stone", "Smoke", "Pond", 
				"River", "Wave", "Sky", "Water", "Tree", "Plant", "Moss", "Flower", "Bush", "Sand", "Mud", 
				"Stars", "Planet", "Mine", "Path", "Road", "Tunnel", "Volcano", "Cave");	
	}

	
}
