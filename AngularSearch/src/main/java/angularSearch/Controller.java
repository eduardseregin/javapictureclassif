package angularSearch;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import angularSearch.items.Item;

public class Controller {

	private List <Integer> ids;

	int iterator = 0;

	public Controller() {
		
	ids = new ArrayList<>();

	}
	
	public void generateJSON () {
		boolean isFinnished = false;
		int count=0;
		while (!isFinnished && iterator<ids.size()) {
			if (count%25==0 || count==10) System.out.println(count + "\n\n" + Item.getAccum());
			if (count%50==0 || count==10) sortAndSave ();
			
			pushItem (generateId ()); // main task
			
			count++;
			isFinnished=Item.size()>=1000 && Item.getAccum().isReached();		
		}
		
	}
	
	private void sortAndSave () {
		Disk.toFile(sort());
	}

	private List <Item> sort () {
		List <Item> list = Item.getItems();
		Collections.sort(list);
		return list;
	}
	
	private void pushItem (int id) {
		BufferedImage image = Pexels.getImage(id);
		String keywords = Pexels.getKeywords(id);
		if (image!=null && keywords!="")
		Item.pushItem (id, keywords, image);
	}
	

	private int generateId () {
		int id = getRandom ();
		while (ids.contains(id)) id = getRandom ();
		ids.add(id);
		return id;
	}


	private int getRandom() {
		return (int) (300000 + Math.random()*7200000);
	}

	
}
