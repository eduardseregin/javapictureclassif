package AngularSearch;

import java.awt.image.BufferedImage;
import java.util.List;

import angularSearch.Pexels;
import angularSearch.items.Item;


public class MainTest {

	      // auto in havana "https://images.pexels.com/photos/3182452/pexels-photo-3182452.jpeg?auto=compress";
	    		  // color fruits "https://images.pexels.com/photos/1092730/pexels-photo-1092730.jpeg?auto=compress";
	    		  // pure b-w-photo "https://images.pexels.com/photos/872756/pexels-photo-872756.jpeg?auto=compress";
	    		  // pastel summer "https://images.pexels.com/photos/937782/pexels-photo-937782.jpeg?auto=compress";
	    		  // sepia winter "https://images.pexels.com/photos/7496128/pexels-photo-7496128.jpeg?auto=compress";
	    		  // sepia higher than pastel "https://images.pexels.com/photos/236397/pexels-photo-236397.jpeg?auto=compress";
	    		  // full aspects sepia "https://images.pexels.com/photos/9324751/pexels-photo-9324751.jpeg?auto=compress";
	    		  // color + pastel, depending "https://images.pexels.com/photos/311300/pexels-photo-311300.jpeg?auto=compress";
	    		  // sepia should have priority over black "https://images.pexels.com/photos/311200/pexels-photo-311200.jpeg?auto=compress";
	    		  // sepia coffee grains "https://images.pexels.com/photos/314957/pexels-photo-314957.jpeg?auto=compress";
	    		  // sepia defined as pastel "https://images.pexels.com/photos/326957/pexels-photo-326957.jpeg?auto=compress";
	    		  // winter "https://images.pexels.com/photos/326056/pexels-photo-326056.jpeg?auto=compress";
	    		  //"https://images.pexels.com/photos/6795725/pexels-photo-6795725.jpeg?auto=compress"; 
	    		  //"https://images.pexels.com/photos/163036/mario-luigi-yoschi-figures-163036.jpeg?auto=compress";
       

	
	public static void main(String[] args) {


		 int id = 3182452;
		    
			String keywords = Pexels.getKeywords(id);
		    BufferedImage image = Pexels.getImage(id);
			
			Item.pushItem (222, keywords, image);
			List <Item> items = Item.getItems();

			System.out.println(items.get(0));
		
	
	}

}
