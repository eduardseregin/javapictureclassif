package AngularSearch;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

import angularSearch.colorAnalysis.ImageCalculator;

import org.junit.Assert;

public class TestCounter {

	ImageCalculator obj;
	Color [] black;
	Color [] sepia;
	Color [] pastel;
	Color [] color;
	
	Color [] autumn;
	Color [] winter;
	Color [] spring;
	
	
	@Before
	public void setObj () {
		obj = new ImageCalculator ();
	}
	

	@Before
	public void setColors () {
		black = new Color [] {new Color (0,0,0), new Color (50,49,48), new Color (250,245,245)};
		sepia = new Color [] {new Color (112,66,20), new Color (35,25,15), new Color (140,120,100)};
		pastel = new Color [] {new Color (240,248,255), new Color (195,251,216), new Color (253,238,217)};
		color = new Color [] {new Color (237,219,17), new Color (21, 86, 237), new Color (230, 21, 90)};
	}
	
	@Before
	public void setSeason () {
		autumn = new Color [] {new Color (255, 99, 71), new Color (255, 215, 0), new Color (139, 69, 19)};
		spring = new Color [] {new Color (173, 255, 47), new Color (21, 198, 80), new Color (0, 168, 0)};
		
	}
	

	@Test
	public void testBlack () {

		String expected = "b1s0p0c0";

		for (Color pix:black) {
		Assert.assertEquals(expected, obj.testPixelColor(pix));
		}
	}
	
	@Test
	public void testSepia () {

		String expected = "b0s1p0c0";

		for (Color pix:sepia) {
		Assert.assertEquals(expected, obj.testPixelColor(pix));
		}
	}
	
	@Test
	public void testPastel () {

		String expected = "b0s0p1c0";

		for (Color pix:pastel) {
		Assert.assertEquals(expected, obj.testPixelColor(pix));
		}
		
	}

	@Test
	public void testColor () {

		String expected = "b0s0p0c1";

		for (Color pix:color) {

		Assert.assertEquals(expected, obj.testPixelColor(pix));
		}
	}
	
	@Test
	public void testAutumn () {

		String expected = "a1s0";
		for (Color pix:autumn) {
		Assert.assertEquals(expected, obj.testPixelAutumnSpring(pix));
		}
	}
	
	
	@Test
	public void testSpring () {

		String expected = "a0s1";
		for (Color pix:spring) {
		Assert.assertEquals(expected, obj.testPixelAutumnSpring(pix));
		}
	}
}
